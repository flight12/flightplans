class FlightOperationChannel < ApplicationCable::Channel
  def subscribed
    stream_from "flight_operation_channel"
  end

  def hello
    puts "hello"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
