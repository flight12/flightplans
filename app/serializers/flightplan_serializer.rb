class FlightplanSerializer < ActiveModel::Serializer
  attributes :id, :departure_aerodrome, :cruising_speed, :destination_aerodrome, :date_of_flight, :aircraft_identification, :total_eet, :persons_on_board, :flight_status
end
