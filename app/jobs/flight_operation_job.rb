class FlightOperationJob < ApplicationJob
  queue_as :default

  def perform(*args)
    # see http://www.icao.int/safety/airnavigation/AIG/Documents/ADREP%20Taxonomy/ECCAIRS%20Aviation%201.3.0.12%20(VL%20for%20AttrID%20%20391%20-%20Event%20Phases).pdf

    f = Flightplan.find_by_id(args[0])

    if f.flight_status == 'standing' || f.flight_status.nil?
      puts 'taxi to runway'
      f.flight_status = 'taxi_to_runway'
      f.save

      FlightOperationJob.set(wait: 2.minute).perform_later f

    elsif f.flight_status == 'taxi_to_runway'
      puts 'takeoff'
      f.flight_status = 'takeoff'
      f.save

      FlightOperationJob.set(wait: 2.minute).perform_later f

    elsif f.flight_status == 'takeoff'
      puts 'climbing - now enroute'
      f.flight_status = 'enroute'
      f.save

      FlightOperationJob.set(wait: (f.total_eet*60-15).minute).perform_later f

    elsif f.flight_status == 'enroute'
      puts 'approach'
      f.flight_status = 'approach'
      f.save

      FlightOperationJob.set(wait: 10.minute).perform_later f

    elsif f.flight_status == 'approach'
      f.flight_status = 'landing'
      f.save

      FlightOperationJob.set(wait: 5.minute).perform_later f

    elsif f.flight_status == 'landing'
      puts 'taxi to gate'
      f.flight_status = 'taxi_from_runway'
      f.save

      FlightOperationJob.set(wait: 2.minute).perform_later f

    elsif f.flight_status == 'taxi_from_runway'
      puts 'completed'
      f.flight_status = 'completed'
      f.save

    end

    ActionCable.server.broadcast "flight_operation_channel", message: f

  end
end
