class Flightplan < ApplicationRecord
  after_save :stream_update

  validates :departure_aerodrome, presence: true
  validates :destination_aerodrome, presence: true
  validates :aircraft_identification, presence: true

  validates :flight_status,
    inclusion: {
      in: %w(standing taxi_to_runway takeoff enroute approach landing taxi_from_runway completed),
      message: "%{value} is not a valid flightStatus"
    }

  validates :date_of_flight, presence: true

  validates :cruising_speed, numericality: { only_integer: true, :greater_than_or_equal_to => 0 }

  def stream_update
    ActionCable.server.broadcast "flight_operation_channel", message: self
  end
end
