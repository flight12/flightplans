class FlightplansController < ApplicationController
  before_action :set_flightplan, only: [:show, :update, :destroy]

  # GET /flightplans
  def index
    @flightplans = Flightplan.all

    render json: @flightplans
  end

  # GET /flightplans/1
  def show
    render json: @flightplan
  end

  # POST /flightplans
  def create
    @flightplan = Flightplan.new(flightplan_params)

    if @flightplan.save
      FlightOperationJob.set(wait_until: @flightplan.date_of_flight).perform_later @flightplan.id

      render json: @flightplan, status: :created, location: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /flightplans/1
  def update
    if @flightplan.update(flightplan_params)
      render json: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # DELETE /flightplans/1
  def destroy
    @flightplan.destroy

    # TODO delete the coresponding FlightOperationJob
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flightplan
      @flightplan = Flightplan.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def flightplan_params
      res = ActiveModelSerializers::Deserialization.jsonapi_parse(params)
      res
    end
end
