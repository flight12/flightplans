server = ENV.fetch("REDIS_SERVICE_HOST","").upcase || 'localhost'
port = ENV.key?("REDIS_SERVICE_PORT") ? ENV["REDIS_SERVICE_PORT"] : 6379

Sidekiq.configure_server do |config|
  config.redis = { url: "redis://#{server}:#{port}" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://#{server}:#{port}" }
end
