require 'sidekiq/web'

Rails.application.routes.draw do
  resources :flightplans

  mount Sidekiq::Web => '/sidekiq'
  mount ActionCable.server => '/cable'

end
