# README

## Prerequisites

We need to run a PostgreSQL database and Sidekiq for background processing, Sidekiq required Redis.

```
rails g scaffold flightplan departure_aerodrome:string cruising_speed:integer destination_aerodrome:string date_of_flight:datetime aircraft_identification:string total_eet:integer persons_on_board:integer flight_status:string
rake db:migrate
```


add some flightplan: `curl -v -H 'Content-Type: application/vnd.api+json' -X POST --data '{"data":{"type":"flightplans","attributes":{"departure-aerodrome":"EDDK","cruising-speed":820,"destination-aerodrome":"EDDF","date-of-flight":"2017-03-18T20:34:00.0Z","aircraft-identification":"X-TEST","total-eet":3,"persons-on-board":5,"flight-status":"standing"}}}' localhost:3000/flightplans`

update: `curl -v -H 'Content-Type: application/vnd.api+json' -X PATCH --data '{"data":{"type":"flightplans","attributes":{"flight-status":"taxi_to_runway"}}}' localhost:3000/flightplans/1`
