class CreateFlightplans < ActiveRecord::Migration[5.0]
  def change
    create_table :flightplans do |t|
      t.string :departure_aerodrome
      t.integer :cruising_speed
      t.string :destination_aerodrome
      t.datetime :date_of_flight
      t.string :aircraft_identification
      t.integer :total_eet
      t.integer :persons_on_board
      t.string :flight_status, :default => "standing"

      t.timestamps
    end
  end
end
