# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

fp1 = Flightplan.create(departure_aerodrome: 'EDDK',
                        cruising_speed: 820,
                        destination_aerodrome: 'EDDF',
                        date_of_flight: Time.now(),
                        aircraft_identification: 'X-TEST',
                        total_eet: 2,
                        persons_on_board: 5)
