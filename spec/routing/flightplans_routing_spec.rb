require "rails_helper"

RSpec.describe FlightplansController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/flightplans").to route_to("flightplans#index")
    end

    it "routes to #show" do
      expect(:get => "/flightplans/1").to route_to("flightplans#show", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/flightplans").to route_to("flightplans#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/flightplans/1").to route_to("flightplans#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/flightplans/1").to route_to("flightplans#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/flightplans/1").to route_to("flightplans#destroy", :id => "1")
    end

  end
end
